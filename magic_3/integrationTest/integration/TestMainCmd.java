package integration;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;

import org.junit.Test;

import actions.ActionHandler;
import actions.DrawCardAction;
import actions.DrawHandAction;
import actions.FillDeckAction;
import actions.PlayCardAction;
import actions.ResetGameAction;
import actions.ShuffleAction;
import model.Card;
import model.Game;
import simulator.Simulator;
import static org.junit.Assert.*;

public class TestMainCmd {

	private static final String FILL_DECK_COMMAND = "fill";
	private static final String PLAY_CARD_COMMAND = "play";
	private static final String SHUFFLE_DECK_COMMAND = "shuffle";
	private static final String DRAW_HAND_COMMAND = "hand";
	private static final String DRAW_COMMAND = "draw";
	private static final String END_OF_FILE = "END";
	private static final String OK_CARD_NAME = "okCard";
	private static final double EXPECTED_PROB = 0.16;
	private static final double TIMES = 100000;
	private static final double DELTA = 0.01d;
	private static final Object A_CAPO = "\n";

	@Test
	public void testMainCmd(){
		
		//Fixture setup
		StringBuilder sb = new StringBuilder();
		sb.append(FILL_DECK_COMMAND);
		sb.append(A_CAPO);
		sb.append(SHUFFLE_DECK_COMMAND);
		sb.append(A_CAPO);
		sb.append(DRAW_COMMAND);
		sb.append(A_CAPO);
		sb.append(PLAY_CARD_COMMAND);
		sb.append(A_CAPO);
		sb.append(END_OF_FILE);
		
		redirectInput(sb.toString());
		
		Card okCard = new Card(OK_CARD_NAME);
		Game game = new Game();
		game.deck.addCard(okCard, 10);
		
		ActionHandler handler = new ActionHandler();
		Simulator sim = new Simulator(handler);
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String line = null;
		try{
			line = br.readLine();
		}catch(Exception e){}
		
		while(line != null && !line.equals(END_OF_FILE)){
			
			switch(line){
			
				case DRAW_COMMAND:
					handler.addAction(new DrawCardAction(game));
					break;
				case DRAW_HAND_COMMAND:
					handler.addAction(new DrawHandAction(game));
					break;
				case SHUFFLE_DECK_COMMAND:
					handler.addAction(new ShuffleAction(game));
					break;
				case PLAY_CARD_COMMAND:
					PlayCardAction playAction = new PlayCardAction(game);
					playAction.setCard(okCard);
					handler.addAction(playAction);	
					break;
				case FILL_DECK_COMMAND:
					handler.addAction(new FillDeckAction(game));
					break;
			}
			try{
				line = br.readLine();
			}catch(Exception e){}
		}
		handler.addAction(new ResetGameAction(game));
		
		//Exercise
		sim.simulate(TIMES);
		double prob = sim.getProb();
		
		//Verify
		assertEquals("value: "+String.valueOf(prob), EXPECTED_PROB, prob, DELTA);
		System.out.println("value: "+String.valueOf(prob));
	}
	
	public static void redirectInput(String s) {
		String userInput = s;
		ByteArrayInputStream in = new ByteArrayInputStream(userInput.getBytes());
		System.setIn(in);
	}
}
