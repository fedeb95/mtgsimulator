package integration;

import static org.junit.Assert.assertEquals;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.assertj.swing.edt.FailOnThreadViolationRepaintManager;
import org.assertj.swing.edt.GuiActionRunner;
import org.assertj.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import model.Game;
import model.MTGModel;
import view.gui.LabelList;
import view.gui.actions.ActionButtonController;
import view.gui.actions.ActionButtonFactory;
import view.gui.actions.ActionButtonList;
import view.gui.deck.CardAdder;
import view.gui.simulator.SimulatorPanel;

public class TestMainGUI {

	private static final int Y_DIM = 600;
	private static final int X_DIM = 1200;
	private static final String CARD_1 = "Arbor Elf";
	private static final String CARD_2 = "Snapcaster Mage";
	private static final String CARD_3 = "Lightning Bolt";
	private static final String SHUFFLE_ACTION_DESC = "Shuffle deck";
	private static final String PLAY_CARD_ACTION_DESC = "Play card";
	private static final String DRAW_CARD_ACTION_DESC = "Draw a card";
	private static final String DRAW_HAND_ACTION_DESC = "Draw " + Game.HAND_SIZE + " cards";
	private static final String GENERIC_ACTION_DESCRIPTION = "action_description";
	private static final int POSSIBLE_ACTIONS_NUMBER = 4;
	private static final String END_DESC = "End";
	FrameFixture window;
	
	@BeforeClass
	public static void setUpOnce() {
	  FailOnThreadViolationRepaintManager.install();
	}
	
	@Before
	public void setUp(){
		
		//Fixture setup
		MTGModel model = MTGModel.getInstance();
		
		ActionButtonController controller = new ActionButtonController(model.getHandler());
		JFrame frame = GuiActionRunner.execute(() -> new JFrame());
		JPanel parentPanel = GuiActionRunner.execute(() -> new JPanel());
		
		ActionButtonList buttonList = GuiActionRunner.execute(() -> 
												new ActionButtonList(model.getGame(), controller));
		LabelList actionLabels = GuiActionRunner.execute(() -> 
												new LabelList());
		
		CardAdder adder = GuiActionRunner.execute(() -> new CardAdder(model.getGame()));
		LabelList cardLabels = GuiActionRunner.execute(() -> 
										new LabelList());
		SimulatorPanel simPane = GuiActionRunner.execute(()-> new SimulatorPanel(model.getSimulator()));
		
		GuiActionRunner.execute(() -> buttonList.createAllSimpleActionButtons());
		GuiActionRunner.execute(() -> buttonList.createPlayCardActionButton(cardLabels, actionLabels));
		
		parentPanel.add(simPane);
		parentPanel.add(buttonList);
		parentPanel.add(actionLabels);
		
		parentPanel.add(adder);
		parentPanel.add(cardLabels);
		
		frame.add(parentPanel);
		
		frame.setPreferredSize(new Dimension(X_DIM,Y_DIM));
		
		window = new FrameFixture(frame);
		window.show();
		
		model.getDeck().addObserver(cardLabels);
		model.getHandler().addObserver(actionLabels);
	}
	
	@Test @Ignore
	public void testLabelList(){
		
		//Exercise
		window.textBox(CardAdder.CARD_NAME_FIELD).enterText(CARD_1);
		window.button(CardAdder.BUTTON_NAME).click();
		window.textBox(CardAdder.CARD_NAME_FIELD).enterText(CARD_2);
		window.button(CardAdder.BUTTON_NAME).click();
		window.textBox(CardAdder.CARD_NAME_FIELD).enterText(CARD_3);
		window.button(CardAdder.BUTTON_NAME).click();
		
		window.button(DRAW_CARD_ACTION_DESC).click();
		window.button(END_DESC).click();
		
		window.textBox(SimulatorPanel.TEXT_FIELD_NAME).setText(""+10000);
		window.button(SimulatorPanel.BUTTON_NAME).click();
		//Verify
		assertEquals("Result: 1.0", window.label(SimulatorPanel.LABEL_NAME).text());
	}
	
	@Test //@Ignore
	public void testActionPlaySelectedCard(){
		
		//Exercise
		window.textBox(CardAdder.CARD_NAME_FIELD).enterText(CARD_1);
		window.button(CardAdder.BUTTON_NAME).click();
		window.button(ActionButtonFactory.ACTION_BUTTON_PREFIX+PLAY_CARD_ACTION_DESC).click();
		window.label(CARD_1).click();
		
		//Verify
		assertEquals(PLAY_CARD_ACTION_DESC + " " + CARD_1, window.label(PLAY_CARD_ACTION_DESC).text());
	}
	
	@After
	public void tearDown() {
	    window.cleanUp();
	}
}
