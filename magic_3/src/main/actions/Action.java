package actions;

import model.Card;

public interface Action{

	public boolean execute();
	
	public String getDescription();

}
