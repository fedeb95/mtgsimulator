package actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class ActionHandler extends Observable implements Action{

	private boolean result;
	List<Action> actionsList;

	public ActionHandler() {
		this.actionsList = new ArrayList<Action>();
		this.result = true;
	}
	
	@Override
	public boolean execute() {
		// permette l'esecuzione di tutti i comandi, incluso il reset
		result = true;
		for (Action action : this.actionsList)
			if (!action.execute()){
				result = false;
			}
			
		return result;
	}

	public void addAction(Action action) {
		this.actionsList.add(action);
		setChanged();
		notifyObservers(action.getDescription());
	}

	@Override
	public String getDescription() {
		return null;
	}

	public void flush() {
		this.actionsList = new ArrayList<Action>();
		this.result = true;
	}
}
