package actions;

import model.Game;

public class DrawCardAction implements Action{

	private Game game;

	public DrawCardAction(Game game) {
		this.game = game;
	}

	@Override
	public boolean execute() {
		game.setClone();
		try{ 
			game.drawCard(); 
		}catch(RuntimeException e){
			return false;
		}
		
		return true;
	}

	public String getDescription(){
		return "Draw a card";
	}
}
