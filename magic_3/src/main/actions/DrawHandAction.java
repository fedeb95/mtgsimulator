package actions;

import model.Game;

public class DrawHandAction implements Action{

	Game game;
	
	public DrawHandAction(Game game) {
		this.game = game;
	}

	@Override
	public boolean execute() {
		game.setClone();
		try{
			this.game.drawStartingHand();
		}catch(RuntimeException e){
			return false;
		}
		return true;
	}
	
	public String getDescription(){
		return "Draw " + game.HAND_SIZE + " cards";
	}

}
