package actions;

import model.Game;

public class FillDeckAction implements Action {
	
	private Game game;

	public FillDeckAction(Game game) {
		this.game = game;
	}

	@Override
	public boolean execute() {
		// set the clone after so the deck filled is cloned.
		// Set even if already set, only a fool would fill it not at the start of the simulation.
		game.fillDeck();
		game.setClone();
		return true;
	}

	@Override
	public String getDescription() {
		
		return "Fill deck";
	}

}
