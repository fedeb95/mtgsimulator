package actions;

import static org.mockito.Mockito.RETURNS_DEEP_STUBS;

import model.Card;
import model.Game;

public class PlayCardAction implements Action{

	Game game;
	Card card;
	
	public PlayCardAction(Game game) {
		this.game = game;
	}

	@Override
	public boolean execute() {
		game.setClone();
		try{
			if (card == null)
				return false;
			
			if (!game.canPlay(card))
				return false;
			
			game.play(card);
		}catch(RuntimeException e){
			return false;
		}
		
		return true;
	}

	public String getDescription() {
		return "Play card";
	}

	public void setCard(Card card) {
		this.card = card;
	}

	public boolean isCardSet() {
		return this.card != null;
	}
	
	public PlayCardAction newInstance(){
		
		return new PlayCardAction(game);
	}
}
