package actions;

import model.Game;

public class ResetGameAction implements Action{

	private Game game;
	
	public ResetGameAction(Game game) {
		this.game = game;
	}
	
	@Override
	public boolean execute() {
		game.setClone();
		game.restart();
		return true;
	}

	@Override
	public String getDescription() {
		return "End";
	}

}
