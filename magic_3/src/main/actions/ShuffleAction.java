package actions;

import model.Game;

public class ShuffleAction implements Action{

	private Game game;
	
	public ShuffleAction(Game game) {
		this.game = game;
	}

	@Override
	public boolean execute() {
		game.setClone();
		try{
			game.shuffleDeck();
		}catch(RuntimeException e){
			return false;
		}
		return true;
	}

	public String getDescription(){
		return "Shuffle deck";
	}
}
