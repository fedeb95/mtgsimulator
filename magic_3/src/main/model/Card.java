package model;

public class Card {

	private String name;
	
	public Card(String name) {
		this.name = name;
	}
	
	public boolean equals(Object o){
		if (!(o instanceof Card))
			return false;
		
		return this.name.equals(((Card)o).name);
	}
	
	public String toString(){
		return this.name;
	}
}
