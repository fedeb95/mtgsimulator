package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EmptyStackException;
import java.util.Observable;
import java.util.Observer;
import java.util.Stack;

public class Deck extends Observable{

	public static final Card NOT_RELEVANT_CARD = new Card("not_relevant");
	public static final int DECK_SIZE = 60;
	ArrayList<Observer> observers = new ArrayList<Observer>();
	Stack<Card> cards;

	public Deck(){
		this.cards = new Stack<Card>();
	}
	
	public Card drawCard() {
		try{
			return cards.pop();
		}catch(EmptyStackException e){
			return null;
		}
	}

	public void addCard(Card card) {
		this.cards.add(card);
		setChanged();
		notifyObservers(card.toString());
	}

	public void shuffle() {
		Collections.shuffle(cards);
	}
	
	public void fill(){
		for (int i = cards.size(); i < DECK_SIZE; i++)
			cards.add(NOT_RELEVANT_CARD);
	}
	
	public Deck clone(){
		Deck clone = new Deck(); 
		for (Card c : cards)
			clone.addCard(c);
		for (Observer o : observers)
			clone.addObserver(o);
		return clone;
	}

	public void addCard(Card card, int amount) {
		for (int i = 0; i < amount; i++)
			addCard(card);
		
	}

	public void reset() {
		cards = new Stack<Card>();
		
	}
	
	@Override
	public void addObserver(Observer o){
		super.addObserver(o);
		observers.add(o);
	}
}
