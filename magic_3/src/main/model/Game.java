package model;

import java.util.ArrayList;
import java.util.List;

public class Game {

	public static final int HAND_SIZE = 7;
	List<Card> cardDrawn;
	public Deck deck;
	private Deck deckClone;
	public List<Card> playGround;

	public Game() {
		this.deck = new Deck();
		this.cardDrawn = new ArrayList<Card>();
		this.playGround = new ArrayList<Card>();
	}

	public void drawCard() {
		Card drawn = this.deck.drawCard();
		if (drawn != null)
			this.cardDrawn.add(drawn);
	}

	public void drawStartingHand() {
		for (int i = 0; i < HAND_SIZE; i++)
			drawCard();
	}

	public void shuffleDeck() {
		this.deck.shuffle();
	}

	public boolean canPlay(Card card) {
		
		return cardDrawn.contains(card);
	}

	public void play(Card card) {
		playGround.add(card);
		cardDrawn.remove(card);
	}
	
	public void restart(){
		if (deckClone != null)
			this.deck = deckClone.clone();
		this.cardDrawn = new ArrayList<Card>();
		this.playGround = new ArrayList<Card>();
	}
	
	public void reset(){
		this.deck.reset();
		this.deckClone = null;
		restart();
	}
	
	public void setClone(){
		if (this.deckClone == null)
			this.deckClone = deck.clone();
	}

	public void fillDeck() {
		deck.fill();
	}

}
