package model;

import actions.ActionHandler;
import simulator.Simulator;

public class MTGModel {

	private Game game;
	private ActionHandler handler;
	private Simulator sim;
	private static MTGModel instance;
	
	private MTGModel(){
		this.game = new Game();
		this.handler = new ActionHandler();
		this.sim = new Simulator(handler);
	}
	
	public static synchronized MTGModel getInstance(){
		
		if (instance == null){
			
			instance = new MTGModel();
		}
		
		return instance;
	}
	
	public Deck getDeck(){
		return game.deck;
	}
	
	public Game getGame(){
		return game;
	}
	
	public ActionHandler getHandler(){
		return handler;
	}
	
	public Simulator getSimulator(){
		return sim;
	}
}
