package simulator;

import actions.ActionHandler;

public class Simulator {

	private ActionHandler handler;
	private double totOKResult = 0;
	private double times;
	private double prob = 0d;

	public Simulator(ActionHandler handler) {
		this.handler = handler;
	}

	public void simulate() {
		if (handler.execute())
			totOKResult+=1;
	}

	public double getProb() {
		return prob;
	}

	public void simulate(double nTimes) {
		this.times = nTimes;
		totOKResult = 0;
		for (int i = 0; i < this.times; i++)
			simulate();
		
		prob = totOKResult/times;
	}
	
	public void reset(){
		this.times = 0;
		this.totOKResult = 0;
		this.handler.flush();
	}

}
