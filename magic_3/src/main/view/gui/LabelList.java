package view.gui;

import java.awt.GridLayout;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

public class LabelList extends JScrollPane implements Observer{
	
	public ArrayList<JLabel> labels;
	MouseListener listener;
	
	public LabelList() {
		super(new JPanel());
		BoxLayout layout = new BoxLayout((JPanel)getViewport().getView(), BoxLayout.Y_AXIS);
		((JPanel)this.getViewport().getView()).setLayout(layout);
		labels = new ArrayList<JLabel>();
	}

	@Override
	public synchronized void addMouseListener(java.awt.event.MouseListener listener) {
		this.listener = listener;
	};
	
	@Override
	public void removeAll(){
		for (JLabel label : labels){
			remove(label);
		}
		labels = new ArrayList<JLabel>();
	}

	@Override
	public void update(Observable arg0,Object text) {
		addNewLabel((String)text);
	}
	

	public void addNewLabel(String text) {
		JLabel label = new JLabel(text);
		label.setBorder(new EmptyBorder(1, 5, 1, 5));
		label.setName(text);
		label.addMouseListener(listener);
		((JPanel)this.getViewport().getView()).add(label);
		this.labels.add(label);
		revalidate();
		repaint();
	}

	public JLabel getLabel(String labelName) {
		for (JLabel l:labels)
			if (l.getName().equals(labelName))
				return l;
		
		return null;
	}
}
