package view.gui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.synth.SynthLookAndFeel;

import model.MTGModel;
import view.gui.actions.ActionButtonController;
import view.gui.actions.ActionButtonList;
import view.gui.deck.ButtonResetDeck;
import view.gui.deck.CardAdder;
import view.gui.simulator.ButtonResetSimulator;
import view.gui.simulator.SimulatorPanel;

public class MTGApp {
	
	private static final int PANEL_BORDER_SPACING = 10;
	private static final String TITLE = "MTG Simulator by Federico Bonfiglio";
	private static final String BUTTON_RESET_SIM_TEXT = "Reset simulator";
	private static final int WIDTH = 700;
	private static final int HEIGHT = 400;
	private static final String BUTTON_RESET_DECK_TEXT = "Empty deck";
	
	MTGModel model;
	ActionButtonController controller;
	JFrame frame;
	JPanel outerPanel, parentPanel, topPane, actionContainer, deckContainer, bottomPane;
	ActionButtonList actionButtons;
	LabelList actionLabels, cardLabels;
	CardAdder adder;
	SimulatorPanel simPane;
	ButtonResetSimulator buttonResetSim;
	ButtonResetDeck buttonResetDeck;
	JLabel topBar;
	
	private MTGApp(){
		
		this.frame = new JFrame();
		this.model = MTGModel.getInstance();
		this.parentPanel = new JPanel();
		this.outerPanel = new JPanel();
		this.topPane = new JPanel();
		this.actionContainer = new JPanel();
		this.deckContainer = new JPanel();
		this.bottomPane = new JPanel();
		
		this.controller = new ActionButtonController(model.getHandler());
		this.actionButtons = new ActionButtonList(model.getGame(), controller);
		this.actionLabels = new LabelList();
		this.cardLabels = new LabelList();
		this.adder = new CardAdder(model.getGame());
		this.simPane = new SimulatorPanel(model.getSimulator());
		this.buttonResetSim = new ButtonResetSimulator(simPane, 
											model.getGame(), actionLabels, BUTTON_RESET_SIM_TEXT);
		this.buttonResetDeck = new ButtonResetDeck(model.getGame(), 
											cardLabels, BUTTON_RESET_DECK_TEXT);
		this.topBar = new JLabel(TITLE);
		
		actionButtons.createAllSimpleActionButtons();
		actionButtons.createPlayCardActionButton(cardLabels, actionLabels);
		model.getDeck().addObserver(cardLabels);
		model.getHandler().addObserver(actionLabels);
	}
	
	private void compose(){
		
		outerPanel.add(actionContainer);
		outerPanel.add(actionLabels);
		outerPanel.add(deckContainer);
		
		topPane.add(topBar);
		actionContainer.add(actionButtons);
		actionContainer.add(actionLabels);
		deckContainer.add(adder);
		deckContainer.add(cardLabels);
		bottomPane.add(simPane);
		bottomPane.add(buttonResetSim);
		bottomPane.add(buttonResetDeck);
		
		parentPanel.add(topPane);
		parentPanel.add(outerPanel);
		parentPanel.add(bottomPane);
		frame.add(parentPanel);
		frame.pack();
		//frame.setSize(new Dimension(WIDTH, HEIGHT));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	private void setLayout(){
		
		Border emptyBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);
		Border etchedBorder = BorderFactory.createEtchedBorder();
		
		BoxLayout parentLayout = new BoxLayout(parentPanel, BoxLayout.PAGE_AXIS);
		parentPanel.setLayout(parentLayout);
		
		BoxLayout outerLayout = new BoxLayout(outerPanel, BoxLayout.LINE_AXIS);
		outerPanel.setLayout(outerLayout);
		
		BoxLayout actionContainerLayout = new BoxLayout(actionContainer, BoxLayout.LINE_AXIS);
		actionContainer.setLayout(actionContainerLayout);
		actionContainer.setBorder(new EmptyBorder(20,20,20,10));
		
		BoxLayout deckContainerLayout = new BoxLayout(deckContainer, BoxLayout.Y_AXIS);
		deckContainer.setLayout(deckContainerLayout);
		deckContainer.setBorder(new EmptyBorder(20,10,20,20));
		
		GridLayout actionButtonLayout = new GridLayout();
		actionButtonLayout.setColumns(2);
		actionButtonLayout.setRows(3);
		actionButtonLayout.setVgap(5);
		actionButtonLayout.setHgap(5);
		actionButtons.setLayout(actionButtonLayout);
		
		actionButtons.setPreferredSize(new Dimension(WIDTH/2, HEIGHT/2));
		actionButtons.setBorder(new EmptyBorder(10,10,10,10));
		actionLabels.setBorder(BorderFactory.createCompoundBorder(emptyBorder, etchedBorder));
		actionLabels.setPreferredSize(new Dimension(170,100));
		
		cardLabels.setPreferredSize(new Dimension(120,220));
		cardLabels.setBorder(BorderFactory.createCompoundBorder(emptyBorder, etchedBorder));
		
		adder.setBorder(emptyBorder);
		topBar.setBorder(new EmptyBorder(20, 0, 0, 0));
		bottomPane.setBorder(new EmptyBorder(0, 0, 20, 0));
	} 
	
	private static void initCustomLookAndFeel() {
	   SynthLookAndFeel lookAndFeel = new SynthLookAndFeel();
	       
	   // SynthLookAndFeel load() method throws a checked exception
	   // (java.text.ParseException) so it must be handled
	   try {
	      lookAndFeel.load(MTGApp.class.getResourceAsStream("look_and_feel.xml"),
	                                                               MTGApp.class);
	      UIManager.setLookAndFeel(lookAndFeel);
	   } 
	   catch (ParseException | UnsupportedLookAndFeelException e) {
	      System.err.println("Couldn't get specified look and feel ("
	                                   + lookAndFeel
	                                    + "), for some reason.");
	      System.err.println("Using the default look and feel.");
	      e.printStackTrace();
	   }
	}
	
	private static void initDefaultLookAndFeel() {
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e) {
		    // If Nimbus is not available, you can set the GUI to another look and feel.
		}
	}
	
	private static void loadFont(){
		try {
		     GraphicsEnvironment ge = 
		         GraphicsEnvironment.getLocalGraphicsEnvironment();
		     ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("MAGIC.TTF")));
		} catch (IOException|FontFormatException e) {
		     System.err.println("Could not load custom font.");
		     e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
		System.setProperty("awt.useSystemAAFontSettings","on");
		System.setProperty("swing.aatext", "true");

		//loadFont(); currently not working
		initDefaultLookAndFeel();
		MTGApp app = new MTGApp();
		app.setLayout();
		app.compose();
		
	}

}
