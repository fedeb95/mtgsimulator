package view.gui;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;

import javax.swing.JFrame;

public class MTGFrame extends JFrame implements ContainerListener {


	@Override
	public void componentAdded(ContainerEvent arg0) {
		revalidate();
		repaint();
	}

	@Override
	public void componentRemoved(ContainerEvent arg0) {
		revalidate();
		repaint();
	}

}
