package view.gui.actions;

import javax.swing.JButton;

import actions.Action;

public class ActionButton extends JButton {

	Action action;
	
	public ActionButton(Action action, String text){
		super(text);
		this.action = action;
	}

	public Action getActionToAdd() {
		return action;
	}
}
