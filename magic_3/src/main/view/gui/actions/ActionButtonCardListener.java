package view.gui.actions;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JLabel;

import actions.Action;
import actions.PlayCardAction;
import model.Card;
import view.gui.LabelList;

public class ActionButtonCardListener extends ActionButton implements MouseListener{

	private PlayCardAction action;
	private LabelList actionLabels;
	private ArrayList<JLabel> alreadySet;
	private Card lastSelected = null;
	private boolean taken = false;
	
	public ActionButtonCardListener(PlayCardAction action, String text, LabelList actionLabels) {
		super(action, text);
		this.action = action;
		this.actionLabels = actionLabels;
		this.alreadySet = new ArrayList<JLabel>();
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {

		if (e.getSource() instanceof JLabel){
			JLabel label = (JLabel) e.getSource();
			for (JLabel l : actionLabels.labels){	// create a builder for this texts
				if (alreadySet.contains(l))
					continue;
				
				
				String text = l.getText() + " ";
				if (text.contains(action.getDescription())){
					alreadySet.add(l);
					l.setText(action.getDescription() + " " + label.getText());
					break;
				}
			}
			
			lastSelected = new Card(label.getText());
			action.setCard(lastSelected);
			if (taken){
				action = action.newInstance();
				taken = false;
			}
			actionLabels.getViewport().getView().revalidate();
			actionLabels.getViewport().getView().repaint();
		}
		
	}

	@Override
	public Action getActionToAdd(){
		taken = true;
		return action;
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
