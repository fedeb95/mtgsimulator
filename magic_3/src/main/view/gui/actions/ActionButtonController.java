package view.gui.actions;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import actions.Action;
import actions.ActionHandler;

public class ActionButtonController implements MouseListener {

	private ActionHandler handler;

	public ActionButtonController(ActionHandler handler){
		this.handler = handler;
	}
	
	@Override
	public void mouseClicked(MouseEvent event) {
		if (!(event.getSource() instanceof ActionButton))
			return;
		
		ActionButton actionButton = (ActionButton)event.getSource();
		Action action = actionButton.getActionToAdd();
		handler.addAction(action);
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
