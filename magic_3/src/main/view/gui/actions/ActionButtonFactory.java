package view.gui.actions;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseListener;

import javax.swing.JComponent;

import actions.Action;
import actions.DrawCardAction;
import actions.DrawHandAction;
import actions.FillDeckAction;
import actions.PlayCardAction;
import actions.ResetGameAction;
import actions.ShuffleAction;
import model.Game;
import view.gui.LabelList;
import view.gui.MTGFrame;

public class ActionButtonFactory {

	public static final String ACTION_BUTTON_PREFIX = "action_button:";
	Game game;
	MouseListener[] listener;
	
	public ActionButtonFactory(Game game, MouseListener... listener) {
		this.game = game;
		this.listener = listener;
	}

	public ActionButton createDrawCardButton() {
		return createButton(new DrawCardAction(game));
	}

	public ActionButton createDrawHandButton() {
		return createButton(new DrawHandAction(game));
	}

	public ActionButton createPlayCardButton(LabelList cardList, LabelList actionLabels) {
		ActionButtonCardListener button =  createButtonListener(new PlayCardAction(game), actionLabels);
		if (cardList != null)
			cardList.addMouseListener(button);
		return button;
	}

	private ActionButtonCardListener createButtonListener(PlayCardAction action, LabelList actionLabels) {
		ActionButtonCardListener button = new ActionButtonCardListener(action, action.getDescription(), actionLabels);
		button.setName(ACTION_BUTTON_PREFIX+action.getDescription());
		for (MouseListener l : listener)
			button.addMouseListener(l);
		return button;
	}

	public ActionButton createShuffleButton() {
		return createButton(new ShuffleAction(game));
	}
	
	public ActionButton createResetButton() {
		return createButton(new ResetGameAction(game));
	}
	
	private ActionButton createButton(Action action){
		ActionButton button =  new ActionButton(action, action.getDescription());
		button.setName(ACTION_BUTTON_PREFIX+action.getDescription());
		for (MouseListener l : listener)
			button.addMouseListener(l);
		return button;
	}

	public ActionButton createFillDeckButton() {
		return createButton(new FillDeckAction(game));
	} 
	
}
