package view.gui.actions;

import java.awt.GridLayout;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

import model.Game;
import view.gui.LabelList;

public class ActionButtonList extends JPanel{

	ActionButtonFactory factory;
	
	public ActionButtonList(Game game, MouseListener... listener){
		
		factory = new ActionButtonFactory(game, listener);
		GridLayout layout = new GridLayout();
		layout.setColumns(1);
		layout.setRows(4);
		layout.setVgap(0);
		this.setLayout(layout);
	}
	
	public void createAllSimpleActionButtons(){
		
		this.add(factory.createDrawCardButton());
		this.add(factory.createDrawHandButton());
		this.add(factory.createShuffleButton());
		this.add(factory.createFillDeckButton());
		this.add(factory.createResetButton());
	}
	
	public void createPlayCardActionButton(LabelList cardList, LabelList actionLabels){
		this.add(factory.createPlayCardButton(cardList, actionLabels));
	}
}
