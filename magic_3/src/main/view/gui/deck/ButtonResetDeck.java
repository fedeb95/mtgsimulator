package view.gui.deck;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import model.Deck;
import model.Game;
import view.gui.LabelList;
import view.gui.simulator.SimulatorPanel;

public class ButtonResetDeck extends JButton implements MouseListener{

	private Game game;
	private LabelList cardLabels;
	
	public ButtonResetDeck(Game game, 
								LabelList cardLabels, String text) {
		super(text);
		this.cardLabels = cardLabels;
		this.game = game;
		this.addMouseListener(this);
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		this.game.reset();
		((JPanel)this.cardLabels.getViewport().getView()).removeAll();
		this.cardLabels.getViewport().getView().revalidate();
		this.cardLabels.getViewport().getView().repaint();
	}
	
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	
}
