package view.gui.deck;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import org.assertj.swing.core.GenericTypeMatcher;

import model.Card;
import model.Deck;
import model.Game;

public class CardAdder extends JPanel implements MouseListener{

	private static final String INITIAL_NUM_VALUE = "1";
	public static final String CARD_NAME_FIELD = "cardAdderTextField";
	public static final String CARD_AMOUNT_FIELD = "cardAountTextField";
	JTextField textField;
	JTextField numField;
	JButton button;
	Game game;
	public static final String BUTTON_NAME = "cardAdderButton";
	
	public CardAdder(Game game){
		this.game = game;
		this.textField = new JTextField();
		this.textField.setPreferredSize(new Dimension(100, 30));
		this.textField.setName(CARD_NAME_FIELD);
		
		this.numField = new JTextField();
		this.numField.setName(CARD_AMOUNT_FIELD);
		this.numField.setPreferredSize(new Dimension(30, 30));
		this.numField.setText(INITIAL_NUM_VALUE);
		
		this.button = new JButton("Add");
		this.button.addMouseListener(this);
		this.button.setName(BUTTON_NAME);
		
		this.add(textField);
		this.add(numField);
		this.add(button);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		String cardName = this.textField.getText();
		Card card = new Card(cardName);
		this.textField.setText("");
		
		int cardNumber = 1;
		try{
			cardNumber = Integer.parseInt(this.numField.getText());
		}catch(NumberFormatException ex){} //remains one
		this.numField.setText(INITIAL_NUM_VALUE);
		
		this.game.deck.addCard(card, cardNumber);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	
}
