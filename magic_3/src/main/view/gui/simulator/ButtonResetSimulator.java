package view.gui.simulator;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import model.Game;
import view.gui.LabelList;

public class ButtonResetSimulator extends JButton implements MouseListener{

	private SimulatorPanel simPane;
	private LabelList actionLabels;
	private Game game;
	
	public ButtonResetSimulator(SimulatorPanel sim, Game game, 
								LabelList actionLabels, String text) {
		super(text);
		this.simPane = sim;
		this.actionLabels = actionLabels;
		this.game = game;
		this.addMouseListener(this);
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		this.simPane.reset();
		((JPanel)this.actionLabels.getViewport().getView()).removeAll();
		this.game.restart();
		this.actionLabels.getViewport().getView().revalidate();
		this.actionLabels.getViewport().getView().repaint();
	}
	
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	
}
