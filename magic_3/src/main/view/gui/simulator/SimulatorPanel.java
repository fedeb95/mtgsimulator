package view.gui.simulator;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import actions.ActionHandler;
import simulator.Simulator;

public class SimulatorPanel extends JPanel implements MouseListener {

	private static final String LABEL_NO_RESULT = "Result:        ";
	private static final String LABEL_RESULT = "Result: ";
	private Simulator simulator;
	private JLabel result;
	private JButton simulateButton;
	private JTextField textField;
	public static final String BUTTON_NAME = "simPaneButton";
	public static final String TEXT_FIELD_NAME = "simPanetextField";
	public static final String LABEL_NAME = "simPaneLabel";
	
	public SimulatorPanel(Simulator sim){
		simulator = sim;
		this.result = new JLabel(LABEL_NO_RESULT);
		this.result.setName(LABEL_NAME);
		this.textField = new JTextField();
		this.textField.setName(TEXT_FIELD_NAME);
		this.textField.setPreferredSize(new Dimension(100, 30));
		this.simulateButton = new JButton("Simulate");
		this.simulateButton.setName(BUTTON_NAME);
		simulateButton.addMouseListener(this);
		this.add(simulateButton);
		this.add(textField);
		this.add(result);
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		int times = 100;
		try{
			times = Integer.parseInt(textField.getText());
		}catch(RuntimeException e){}
	
		simulator.simulate(times);
		result.setText(LABEL_RESULT + String.valueOf(simulator.getProb()));
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void reset() {
		this.simulator.reset();
		this.result.setText(LABEL_NO_RESULT);
		this.revalidate();
		this.repaint();
	}
}
