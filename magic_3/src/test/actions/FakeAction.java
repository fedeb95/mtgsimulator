package actions;

import actions.Action;

public class FakeAction implements Action {

	@Override
	public boolean execute() {
		return true;
	}

	@Override
	public String getDescription() {
		
		return "fake_description";
	}

}
