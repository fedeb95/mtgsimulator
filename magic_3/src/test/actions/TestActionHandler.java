package actions;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.junit.Test;

import actions.Action;
import actions.ActionHandler;

public class TestActionHandler {

	@Test
	public void testOnlyOneSuccessfulAction(){
		
		//Fixture setup
		ActionHandler handler = new ActionHandler();
		Action mockedAction = createMockedAction(true);
		
		//Exercise
		handler.addAction(mockedAction);
		boolean result = handler.execute();
		
		//Verify
		assertTrue(result);
		verify(mockedAction).execute();
	}
	
	@Test
	public void testMoreSuccesfulActions(){
		
		//Fixture setup
		ActionHandler handler = createHandlerWithActions(3, 0);
		
		//Exercise
		boolean result = handler.execute();
		
		assertTrue(result);
		verifyMockedActions(handler);
		
	}
	
	@Test
	public void testOneFaultingAction(){
		
		//Fixture setup
		ActionHandler handler = createHandlerWithActions(2, 1);
		
		//Exercise
		boolean result = handler.execute();
		
		//Verify
		assertFalse(result);
		verifyMockedActions(handler);
	}

	private Action createMockedAction(boolean succesful) {
		Action mockAction = mock(FakeAction.class);
		when(mockAction.execute()).thenReturn(succesful);
		return mockAction;
	}
	
	private ActionHandler createHandlerWithActions(int numberOfOkActions, int numberOfFaultAction){
		
		ActionHandler handler = new ActionHandler();
		for (int i = 0; i < numberOfOkActions; i++)
			handler.addAction(createMockedAction(true));
		for (int i = 0; i < numberOfFaultAction; i++)
			handler.addAction(createMockedAction(false));
		
		return handler;
	}
	
	private void verifyMockedActions(ActionHandler handler){
		for (Action action : handler.actionsList)
			verify(action).execute();
	}
}
