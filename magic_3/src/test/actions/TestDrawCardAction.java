package actions;

import actions.DrawCardAction;
import model.Game;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestDrawCardAction {
	
	@Test
	public void testDrawCardActionSuccessful(){
		
		//Fixture setup
		Game game = mock(Game.class);
		Action drawAction = new DrawCardAction(game);
		
		//Exercise
		boolean result = drawAction.execute();
		
		//Verify
		assertTrue(result);
		verify(game).drawCard();
	}
	
	@Test
	public void testDrawCardActionNoMoreCards(){
		
		//Fixture setup
		Game game = mock(Game.class);
		Action drawAction = new DrawCardAction(game);
		doThrow(new RuntimeException()).when(game).drawCard();
		
		//Exercise
		boolean result = drawAction.execute();
		
		//Verify
		assertFalse(result);
		verify(game).drawCard();
	}

	

}
