package actions;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Test;

import model.Game;

public class TestDrawHandAction {

	@Test
	public void testDrawHandSuccesful(){
		
		//Fixture setup
		Game mockedGame = mock(Game.class);
		Action handAction = new DrawHandAction(mockedGame);
		
		//Exercise
		boolean result = handAction.execute();
		
		//Verify
		assertTrue(result);
		verify(mockedGame).drawStartingHand();
	}
	
	@Test
	public void testDrawHandFaulting(){
		
		//Fixture setup
		Game mockedGame = mock(Game.class);
		Action handAction = new DrawHandAction(mockedGame);
		doThrow(RuntimeException.class).when(mockedGame).drawStartingHand();
		
		//Exercise
		boolean result = handAction.execute();
		
		//Verify
		assertFalse(result);
		verify(mockedGame).drawStartingHand();
	} 
}
