package actions;

import org.junit.Test;
import static org.junit.Assert.*;
import model.Deck;
import model.Game;

public class TestFillDeckAction {
	
	@Test
	public void testFillDeck(){
		
		//Fixture setup
		Game game = new Game();
		Action fillAction = new FillDeckAction(game);
		
		//Exercise
		fillAction.execute();
		
		//Verify
		for (int i = 0; i < Deck.DECK_SIZE; i++){
			game.drawCard();
			assertTrue(game.canPlay(Deck.NOT_RELEVANT_CARD));
			game.play(Deck.NOT_RELEVANT_CARD);
		}
	}

}
