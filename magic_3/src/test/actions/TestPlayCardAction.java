package actions;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;

import model.Card;
import model.Game;

public class TestPlayCardAction {

	@Test
	public void testPlaySuccessful(){
		
		//Fixture setup
		Game mockedGame = mock(Game.class);
		Card mockedCard = mock(Card.class);
		when(mockedGame.canPlay(mockedCard)).thenReturn(true);
		PlayCardAction playAction = new PlayCardAction(mockedGame);
		playAction.setCard(mockedCard);		
		
		//Exercise
		boolean result = playAction.execute();
				
		//Verify
		assertTrue(result);
		verify(mockedGame).canPlay(mockedCard);
		verify(mockedGame).play(mockedCard);
	}
	
	@Test
	public void testPlayFaulting(){
		
		//Fixture setup
				Game mockedGame = mock(Game.class);
				Card mockedCard = mock(Card.class);
				when(mockedGame.canPlay(mockedCard)).thenReturn(false);
				PlayCardAction playAction = new PlayCardAction(mockedGame);
				playAction.setCard(mockedCard);		
				
				//Exercise
				boolean result = playAction.execute();
						
				//Verify
				assertFalse(result);
				verify(mockedGame).canPlay(mockedCard);
				verify(mockedGame, times(0)).play(mockedCard);
	}
	
	@Test
	public void testPlayFaultingDueToNoCard(){
		
		//Fixture setup
				Game mockedGame = mock(Game.class);
				Card mockedCard = mock(Card.class);
				when(mockedGame.canPlay(mockedCard)).thenReturn(true);
				PlayCardAction playAction = new PlayCardAction(mockedGame);	
				
				//Exercise
				boolean result = playAction.execute();
						
				//Verify
				assertFalse(result);
				verify(mockedGame, times(0)).canPlay(mockedCard);
				verify(mockedGame, times(0)).play(mockedCard);
	}
}
