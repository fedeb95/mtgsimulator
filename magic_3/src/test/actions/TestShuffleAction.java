package actions;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Test;

import model.Game;

public class TestShuffleAction {

	@Test
	public void testShuffleSuccessful(){
		
		//Fixture setup
		Game game = mock(Game.class);
		Action shuffle = new ShuffleAction(game);
		
		//Exercise
		boolean result = shuffle.execute();
		
		//Verify
		assertTrue(result);
		verify(game, times(1)).shuffleDeck();
	}
	
	@Test
	public void testShuffleFaulting(){
		
		//Fixture setup
		Game game = mock(Game.class);
		doThrow(RuntimeException.class).when(game).shuffleDeck();
		Action shuffle = new ShuffleAction(game);
				
		//Exercise
		boolean result = shuffle.execute();
				
		//Verify
		assertFalse(result);
		verify(game, times(1)).shuffleDeck();
	}
}
