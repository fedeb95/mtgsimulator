package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class TestDeck {

	private static final String GENERIC_CARD_NAME = "generic_card";
	
	@Test
	public void testDrawCard(){
		
		//Fixture setup
		Card card = new Card(GENERIC_CARD_NAME);
		Deck deck = new Deck();
		deck.addCard(card);
		
		//Exercise
		Card cardDrawn = deck.drawCard();
		
		//Verify
		assertEquals(card, cardDrawn);
	}
	
	@Test
	public void testFill(){
		
		//Fixture setup
		Card card = new Card(GENERIC_CARD_NAME);
		Deck deck = new Deck();
		deck.addCard(card);
			
		//Exercise
		deck.fill();
				
		//Verify
		assertEquals(Deck.DECK_SIZE, deck.cards.size());
	}
	
	@Test
	public void testShuffle(){
		
		//one time over sixty fails: must shuffle randomly
		
		//Fixture setup
		Card card = new Card(GENERIC_CARD_NAME);
		Deck deck = new Deck();
		deck.fill();
		deck.cards.add(card);
				
		//Exercise
		deck.shuffle();
		Card firstCard = deck.drawCard();
				
		//Verify
		assertNotEquals(card, firstCard);
	}
}
