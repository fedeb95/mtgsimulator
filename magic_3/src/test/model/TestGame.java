package model;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Test;

public class TestGame {
	
	private static final int HAND_SIZE = 7;

	@Test
	public void testDrawCard(){
		
		//Fixture setup
		Game game = new Game();
		game.deck.addCard(Deck.NOT_RELEVANT_CARD);
		
		//Exercise
		game.drawCard();
		
		//Verify
		assertTrue(game.canPlay(Deck.NOT_RELEVANT_CARD));
		
	}
	
	@Test
	public void testDrawStartingHand(){
	
		//Fixture setup
		Game game = new Game();
		for (int i = 0; i < HAND_SIZE; i++)
			game.deck.addCard(Deck.NOT_RELEVANT_CARD);
		
		//Exercise
		game.drawStartingHand();
		
		//Verify
		for (int i = 0; i < HAND_SIZE; i++){
			game.drawCard();
			assertTrue(game.canPlay(Deck.NOT_RELEVANT_CARD));
			game.play(Deck.NOT_RELEVANT_CARD);
		}
	}
	
	@Test
	public void testPlayCardNotDrawn(){
		
		//Fixture setup
		Game game = new Game();
		game.deck.addCard(Deck.NOT_RELEVANT_CARD);
		
		//Exercise
		
		
		//Verify
		assertFalse(game.canPlay(Deck.NOT_RELEVANT_CARD));
	}
}
