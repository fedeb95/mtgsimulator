package simulator;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Test;

import actions.ActionHandler;

public class TestSimulator {

	private static final double EXPECTED_PROB = 0.9d;
	private static final double DELTA = 0.01;
	private static final int N_TIMES = 10;

	@Test
	public void testSimulateOnce(){
		
		//Fixture setup
		ActionHandler mockedHandler = mock(ActionHandler.class);
		Simulator sim = new Simulator(mockedHandler);
		
		//Exercise
		sim.simulate();
		
		//Verify
		verify(mockedHandler, times(1)).execute();
	}
	
	@Test
	public void testSimulateNTimes(){
		
		//Fixture setup
		ActionHandler mockedHandler = mock(ActionHandler.class);
		when(mockedHandler.execute()).thenReturn(false).thenReturn(true);
		Simulator sim = new Simulator(mockedHandler);
		
		//Exercise
		sim.simulate(N_TIMES);
		
		//Verify
		verify(mockedHandler, times(N_TIMES)).execute();
		assertEquals(EXPECTED_PROB, sim.getProb(), DELTA);
	}
}
