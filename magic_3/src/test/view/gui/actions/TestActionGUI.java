package view.gui.actions;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.awt.Dimension;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.assertj.swing.edt.FailOnThreadViolationRepaintManager;
import org.assertj.swing.edt.GuiActionRunner;
import org.assertj.swing.fixture.FrameFixture;
import org.assertj.swing.fixture.JButtonFixture;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Matchers;

import actions.Action;
import actions.ActionHandler;
import model.Game;

public class TestActionGUI {

	private static final int Y_DIM = 600;
	private static final int X_DIM = 1000;
	private static final String SHUFFLE_ACTION_DESC = "Shuffle deck";
	private static final String PLAY_CARD_ACTION_DESC = "Play card";
	private static final String DRAW_CARD_ACTION_DESC = "Draw a card";
	private static final String DRAW_HAND_ACTION_DESC = "Draw " + Game.HAND_SIZE + " cards";
	private static final String GENERIC_ACTION_DESCRIPTION = "action_description";
	private static final int POSSIBLE_ACTIONS_NUMBER = 4;
	private FrameFixture window;
	
	@BeforeClass
	public static void setUpOnce() {
	  FailOnThreadViolationRepaintManager.install();
	}
	
	public void setUp(JComponent comp){
		
		JFrame frame = GuiActionRunner.execute(() -> new JFrame());
		JPanel panel = GuiActionRunner.execute(() -> new JPanel());
		
		panel.add(comp);
		frame.add(panel);
		frame.pack();
		
		frame.setPreferredSize(new Dimension(X_DIM,Y_DIM));
		frame.setVisible(true);
		window = new FrameFixture(frame);
		window.show();
		
	}
	
	@Test
	public void testAddOneActionButton(){
		
		//Fixture setup
		Action mockedAction = mock(Action.class);
		ActionButton actionButton = GuiActionRunner.execute(() -> new ActionButton(mockedAction,
																	GENERIC_ACTION_DESCRIPTION));
		setUp(actionButton);
		JButtonFixture button = window.button();
		
		//Verify
		assertEquals(GENERIC_ACTION_DESCRIPTION, button.text());
	}
	
	@Test
	public void testActionAddedOnCLick(){

		//Fixture setup
		ActionHandler handler = new ActionHandler();
		ActionHandler spyHandler = spy(handler);
		ActionButtonController controller = new ActionButtonController(spyHandler);
		ActionButton actionButton = GuiActionRunner.execute(() -> new ActionButton(mock(Action.class),
																	GENERIC_ACTION_DESCRIPTION));
		actionButton.addMouseListener(controller);
		setUp(actionButton);
		JButtonFixture button = window.button();
		
		//Exercise
		button.click();
				
		//Verify
		verify(spyHandler).addAction(Matchers.<Action>any());
	}
	
	@Test //@Ignore
	public void testActionButtonList(){
		//Fixture setup
		ActionHandler handler = new ActionHandler();
		ActionHandler spyHandler = spy(handler);
		ActionButtonController controller = new ActionButtonController(spyHandler);
		Game mockedGame = mock(Game.class);
		ActionButtonList buttonList = GuiActionRunner.execute(() -> 
										new ActionButtonList(mockedGame, controller));		
		setUp(buttonList);
		
		//Exercise
		clickAllButtons();
		
		//Verify
		verify(spyHandler, times(POSSIBLE_ACTIONS_NUMBER)).addAction(Matchers.<Action>any());
		
	}
	
	@Test
	public void testPlayCardButton(){
	
	}

	private void clickAllButtons() {
		window.button(DRAW_CARD_ACTION_DESC).click();
		window.button(DRAW_HAND_ACTION_DESC).click();
		window.button(PLAY_CARD_ACTION_DESC).click();
		window.button(SHUFFLE_ACTION_DESC).click();
	}
	
	@After
	public void tearDown() {
	    window.cleanUp();
	}
}
