package view.gui.actions;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.awt.Dimension;

import javax.swing.JPanel;

import org.assertj.swing.edt.FailOnThreadViolationRepaintManager;
import org.assertj.swing.edt.GuiActionRunner;
import org.assertj.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Matchers;

import actions.Action;
import actions.ActionHandler;
import model.Game;
import view.gui.LabelList;
import view.gui.MTGFrame;

public class TestActionLabelListGUI {

	private static final int Y_DIM = 600;
	private static final int X_DIM = 1000;
	private static final String BUTTON_PREFIX = "action_button:";
	private static final String SHUFFLE_ACTION_DESC = "Shuffle deck";
	private static final String PLAY_CARD_ACTION_DESC = "Play card";
	private static final String DRAW_CARD_ACTION_DESC = "Draw a card";
	private static final String FILL_DECK_ACTION_DESC = "Fill deck";
	private static final String DRAW_HAND_ACTION_DESC = "Draw " + Game.HAND_SIZE + " cards";
	private static final String GENERIC_ACTION_DESCRIPTION = "action_description";
	private static final int POSSIBLE_ACTIONS_NUMBER = 5;
	FrameFixture window;
	
	@BeforeClass
	public static void setUpOnce() {
	  FailOnThreadViolationRepaintManager.install();
	}
	
	
	@Before
	public void setUp(){
		
		
	}
	@Test
	public void testLabelList(){
		
		//Fixture setup
		ActionHandler handler = new ActionHandler();
		ActionHandler spyHandler = spy(handler);
		ActionButtonController controller = new ActionButtonController(spyHandler);
		Game mockedGame = mock(Game.class);
	
		MTGFrame frame = GuiActionRunner.execute(() -> new MTGFrame());
		JPanel parentPanel = GuiActionRunner.execute(() -> new JPanel());
		ActionButtonList buttonList = GuiActionRunner.execute(() -> 
												new ActionButtonList(mockedGame, controller));
		LabelList labelList = GuiActionRunner.execute(() -> 
										new LabelList());
		GuiActionRunner.execute(() -> buttonList.createAllSimpleActionButtons());
		GuiActionRunner.execute(() -> buttonList.createPlayCardActionButton(null, labelList));
		
		//labelList.addContainerListener(frame);
		parentPanel.add(buttonList);
		parentPanel.add(labelList);
		
		frame.add(parentPanel);
		frame.pack();
		
		frame.setPreferredSize(new Dimension(X_DIM,Y_DIM));
		frame.setVisible(true);
		window = new FrameFixture(frame);
		window.show();
		spyHandler.addObserver(labelList);
		
		//Exercise
		clickAllButtons();
		
		//Verify
		verify(spyHandler, times(POSSIBLE_ACTIONS_NUMBER)).notifyObservers(Matchers.<Action>any());
		assertEquals(DRAW_CARD_ACTION_DESC, window.label(DRAW_CARD_ACTION_DESC).text());
		assertEquals(DRAW_HAND_ACTION_DESC, window.label(DRAW_HAND_ACTION_DESC).text());
		assertEquals(PLAY_CARD_ACTION_DESC, window.label(PLAY_CARD_ACTION_DESC).text());
		assertEquals(SHUFFLE_ACTION_DESC, window.label(SHUFFLE_ACTION_DESC).text());
		assertEquals(FILL_DECK_ACTION_DESC, window.label(FILL_DECK_ACTION_DESC).text());
	}
	
	private void clickAllButtons() {
		window.button(BUTTON_PREFIX+DRAW_CARD_ACTION_DESC).click();
		window.button(BUTTON_PREFIX+DRAW_HAND_ACTION_DESC).click();
		window.button(BUTTON_PREFIX+PLAY_CARD_ACTION_DESC).click();
		window.button(BUTTON_PREFIX+SHUFFLE_ACTION_DESC).click();
		window.button(BUTTON_PREFIX+FILL_DECK_ACTION_DESC).click();
	}
	
	@After
	public void tearDown() {
	    window.cleanUp();
	}
}
