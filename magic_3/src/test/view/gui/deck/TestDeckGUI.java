package view.gui.deck;

import java.awt.Dimension;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.assertj.swing.edt.FailOnThreadViolationRepaintManager;
import org.assertj.swing.edt.GuiActionRunner;
import org.assertj.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import model.Card;
import model.Game;

public class TestDeckGUI {

	private static final String CARD_NAME = "Arbor Elf";
	private static final int CARD_AMOUNT = 4;
	FrameFixture window;
	
	@BeforeClass
	public static void setUpOnce() {
	  FailOnThreadViolationRepaintManager.install();
	}
	
	public void setUp(JComponent comp){
		
		JFrame frame = GuiActionRunner.execute(() -> new JFrame());
		JPanel panel = GuiActionRunner.execute(() -> new JPanel());
		
		panel.add(comp);
		frame.add(panel);
		frame.pack();
		
		frame.setPreferredSize(new Dimension(300,600));
		frame.setVisible(true);
		window = new FrameFixture(frame);
		window.show();
		
	}
	
	@Test
	public void testCardAdder(){
		
		//Fixture setup
		Game game = new Game();
		CardAdder adder = GuiActionRunner.execute(() -> new CardAdder(game));
		setUp(adder);
		
		//Exercise
		window.textBox(CardAdder.CARD_NAME_FIELD).enterText(CARD_NAME);
		window.textBox(CardAdder.CARD_AMOUNT_FIELD).setText("");
		window.textBox(CardAdder.CARD_AMOUNT_FIELD).enterText("4");
		window.button(CardAdder.BUTTON_NAME).click();
		game.drawCard();
		game.drawCard();
		game.drawCard();
		game.drawCard();
		
		//Verify
		Card card = new Card(CARD_NAME);
		
		for (int i = 0; i < CARD_AMOUNT; i++){
			assertTrue(game.canPlay(card)); 
			game.play(card);
		}
		assertFalse(game.canPlay(card));
	}
	
	@After
	public void tearDown() {
	    window.cleanUp();
	}
}
