package view.gui.deck;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.awt.Dimension;

import javax.swing.JPanel;

import org.assertj.swing.edt.FailOnThreadViolationRepaintManager;
import org.assertj.swing.edt.GuiActionRunner;
import org.assertj.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import model.Card;
import model.Game;
import view.gui.LabelList;
import view.gui.MTGFrame;

public class TestDeckLabelListGUI {

	/**
	 * EDT violation ? The production app works well
	 * */
	
	private static final int Y_DIM = 600;
	private static final int X_DIM = 1000;
	private static final String CARD_1 = "Arbor Elf";
	private static final String CARD_2 = "Snapcaster Mage";
	private static final String CARD_3 = "Lightning Bolt";
	FrameFixture window;
	
	@BeforeClass
	public static void setUpOnce() {
	  FailOnThreadViolationRepaintManager.install();
	}
	
	@Test
	public void testLabelList(){
		
		//Fixture setup
		Game game = spy(new Game());
		MTGFrame frame = GuiActionRunner.execute(() -> new MTGFrame());
		JPanel parentPanel = GuiActionRunner.execute(() -> new JPanel());
		CardAdder adder = GuiActionRunner.execute(() -> new CardAdder(game));
		LabelList labelList = GuiActionRunner.execute(() -> 
										new LabelList());
		
		labelList.addContainerListener(frame);
		parentPanel.add(adder);
		parentPanel.add(labelList);
		
		frame.add(parentPanel);
		frame.pack();
		
		frame.setPreferredSize(new Dimension(X_DIM,Y_DIM));
		frame.setVisible(true);
		window = new FrameFixture(frame);
		window.show();
		game.deck.addObserver(labelList);
		
		//Exercise
		window.textBox(CardAdder.CARD_NAME_FIELD).enterText(CARD_1);
		window.button(CardAdder.BUTTON_NAME).click();
		window.textBox(CardAdder.CARD_NAME_FIELD).enterText(CARD_2);
		window.button(CardAdder.BUTTON_NAME).click();
		window.textBox(CardAdder.CARD_NAME_FIELD).enterText(CARD_3);
		window.button(CardAdder.BUTTON_NAME).click();
		
		//Verify
		verify(game.deck).addCard(new Card(CARD_1));
		assertEquals(CARD_1, window.label(CARD_1).text());
		verify(game.deck).addCard(new Card(CARD_2));
		assertEquals(CARD_2, window.label(CARD_2).text());
		verify(game.deck).addCard(new Card(CARD_3));
		assertEquals(CARD_3, window.label(CARD_3).text());
	}
	
	@After
	public void tearDown() {
	    window.cleanUp();
	}
}
