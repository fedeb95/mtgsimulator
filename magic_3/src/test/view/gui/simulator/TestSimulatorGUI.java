package view.gui.simulator;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import java.awt.Dimension;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.assertj.swing.edt.FailOnThreadViolationRepaintManager;
import org.assertj.swing.edt.GuiActionRunner;
import org.assertj.swing.fixture.FrameFixture;
import org.assertj.swing.fixture.JLabelFixture;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Matchers;

import simulator.Simulator;

public class TestSimulatorGUI {

	private static final int TIMES = 10;
	private FrameFixture window;
	
	@BeforeClass
	public static void setUpOnce() {
	  FailOnThreadViolationRepaintManager.install();
	}
	
	public void setUp(JComponent comp){
		
		JFrame frame = GuiActionRunner.execute(() -> new JFrame());
		JPanel panel = GuiActionRunner.execute(() -> new JPanel());
		
		panel.add(comp);
		frame.add(panel);
		frame.pack();
		
		frame.setPreferredSize(new Dimension(300,600));
		frame.setVisible(true);
		window = new FrameFixture(frame);
		window.show();
		
	}
	
	@Test
	public void testSimulationStart(){
		
		//Fixture setup
		Simulator sim = mock(Simulator.class);
		when(sim.getProb()).thenReturn(0d);
		doNothing().when(sim).simulate();
		SimulatorPanel simPane = GuiActionRunner.execute(() -> new SimulatorPanel(sim));
		setUp(simPane);
		
		//Exercise
		window.textBox().setText(""+TIMES);
		window.button().click();
		
		//Verify
		JLabelFixture label = window.label();
		assertEquals("Result: 0.0", label.text());
		verify(sim).simulate(TIMES);
		verify(sim, times(1)).getProb();
	}
	
	@After
	public void tearDown() {
	    window.cleanUp();
	}
}
